import pytest

from adder import add


def test_that_positive_numbers_can_be_added():
    assert add(2, 3) == 5


@pytest.mark.slow
def test_that_numbers_can_be_doubled_by_adding():
    for number in range(-1000000, 1000000):
        assert add(number, number) == number * 2
