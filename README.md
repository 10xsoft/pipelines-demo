# Mergetastic and Bitbucket Pipelines Integration Demo

This repository demos how to integrate Bitbucket Pipelines with
[Mergetastic](https://mergetastic.com). A step-by-step walkthrough of this
integration can be found
[here](https://mergetastic.com/docs/bitbucket-pipelines/).
